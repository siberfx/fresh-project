<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_tr');
            $table->string('title_en');
            $table->string('title_de');
            $table->string('title_fr');
            $table->string('title_ru');
            $table->text('content_tr');
            $table->text('content_en');
            $table->text('content_de');
            $table->text('content_fr');
            $table->text('content_ru');
            $table->string('keyword_tr');
            $table->string('keyword_en');
            $table->string('keyword_de');
            $table->string('keyword_fr');
            $table->string('keyword_ru');
            $table->string('slug_tr')->unique();
            $table->string('slug_en')->unique();
            $table->string('slug_de')->unique();
            $table->string('slug_fr')->unique();
            $table->string('slug_ru')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about');
    }
}
