<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('address');
            $table->string('email');
            $table->string('phone');
            $table->string('mobile');
            $table->string('copyright');
            $table->timestamps();

        });
    }
    public function down()
    {
        Schema::dropIfExists('contact');
    }
}
