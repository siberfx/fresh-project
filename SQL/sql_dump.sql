-- --------------------------------------------------------
-- Sunucu:                       localhost
-- Sunucu sürümü:                5.7.19 - MySQL Community Server (GPL)
-- Sunucu İşletim Sistemi:       Win64
-- HeidiSQL Sürüm:               9.5.0.5278
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- tablo yapısı dökülüyor forlaravel.about
CREATE TABLE IF NOT EXISTS `about` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_tr` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_fr` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_tr` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_fr` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyword_tr` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyword_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyword_fr` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug_tr` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug_fr` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `about_slug_tr_unique` (`slug_tr`),
  UNIQUE KEY `about_slug_en_unique` (`slug_en`),
  UNIQUE KEY `about_slug_fr_unique` (`slug_fr`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- forlaravel.about: 1 rows tablosu için veriler indiriliyor
DELETE FROM `about`;
/*!40000 ALTER TABLE `about` DISABLE KEYS */;
INSERT INTO `about` (`id`, `title_tr`, `title_en`, `title_fr`, `content_tr`, `content_en`, `content_fr`, `keyword_tr`, `keyword_en`, `keyword_fr`, `slug_tr`, `slug_en`, `slug_fr`, `created_at`, `updated_at`) VALUES
	(1, 'Başlık', 'Title', 'Title FR', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'kelime tr', 'kelime en', 'kelime fr', 'baslik', 'slug-en	', 'slug-fr', '2018-05-16 19:16:58', '2018-05-16 19:16:59');
/*!40000 ALTER TABLE `about` ENABLE KEYS */;

-- tablo yapısı dökülüyor forlaravel.blog_categories
CREATE TABLE IF NOT EXISTS `blog_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `editor_id` int(10) unsigned NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `blog_categories_slug_unique` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- forlaravel.blog_categories: 0 rows tablosu için veriler indiriliyor
DELETE FROM `blog_categories`;
/*!40000 ALTER TABLE `blog_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog_categories` ENABLE KEYS */;

-- tablo yapısı dökülüyor forlaravel.blog_content
CREATE TABLE IF NOT EXISTS `blog_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `editor_id` int(10) unsigned NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `blog_content_slug_unique` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- forlaravel.blog_content: 0 rows tablosu için veriler indiriliyor
DELETE FROM `blog_content`;
/*!40000 ALTER TABLE `blog_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog_content` ENABLE KEYS */;

-- tablo yapısı dökülüyor forlaravel.companies
CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `editor_id` int(10) unsigned NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `companies_slug_unique` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- forlaravel.companies: 0 rows tablosu için veriler indiriliyor
DELETE FROM `companies`;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` (`id`, `title`, `content`, `description`, `keywords`, `editor_id`, `slug`, `created_at`, `updated_at`) VALUES
	(1, 'Ertok Yapı Malzemeleri', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'versions, lorem, ipsum, keywords', 1, 'ertok-yapi-malzemeleri', '2018-05-20 20:33:21', '2018-05-20 20:33:23'),
	(2, 'Yemenoğulları Hırdavat', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'versions, lorem, ipsum, keywords', 1, 'yemenogullari-hirdavat', '2018-05-20 20:33:21', '2018-05-20 20:33:23'),
	(3, 'Yemenoğlu Gayrimenkul', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'versions, lorem, ipsum, keywords', 1, 'yemenoglu-gayrimenkul', '2018-05-20 20:33:21', '2018-05-20 20:33:23'),
	(4, 'Öztaş Gayrimenkul', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'versions, lorem, ipsum, keywords', 1, 'oztas-gayrimenkul', '2018-05-20 20:33:21', '2018-05-20 20:33:23'),
	(5, 'Ges Madencilik', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'versions, lorem, ipsum, keywords', 1, 'ges-madencilik', '2018-05-20 20:33:21', '2018-05-20 20:33:23'),
	(6, 'Ertok Sigorta', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'versions, lorem, ipsum, keywords', 1, 'ertok-sigorta', '2018-05-20 20:33:21', '2018-05-20 20:33:23');
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;

-- tablo yapısı dökülüyor forlaravel.companies_detail
CREATE TABLE IF NOT EXISTS `companies_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(10) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mission` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `vision` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `editor_id` int(10) unsigned NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `companies_detail_slug_unique` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- forlaravel.companies_detail: 0 rows tablosu için veriler indiriliyor
DELETE FROM `companies_detail`;
/*!40000 ALTER TABLE `companies_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `companies_detail` ENABLE KEYS */;

-- tablo yapısı dökülüyor forlaravel.contact
CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `copyright` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- forlaravel.contact: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `contact`;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` (`id`, `title`, `address`, `email`, `phone`, `mobile`, `copyright`, `created_at`, `updated_at`) VALUES
	(1, 'SiberFx Demo Page Design.', 'Hrodnenskaya Oblast, Uritskogo 14-1, Grodno, 220023, Belarus', 'info@siberfx.com', '+375255150806', '+375255150806', 'Made by: Selim GORMUS  ||  itransition Demo', '2018-05-07 13:33:44', '2018-05-07 17:09:30');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;

-- tablo yapısı dökülüyor forlaravel.kryptonit3_counter_page
CREATE TABLE IF NOT EXISTS `kryptonit3_counter_page` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `page` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kryptonit3_counter_page_page_unique` (`page`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- forlaravel.kryptonit3_counter_page: 3 rows tablosu için veriler indiriliyor
DELETE FROM `kryptonit3_counter_page`;
/*!40000 ALTER TABLE `kryptonit3_counter_page` DISABLE KEYS */;
INSERT INTO `kryptonit3_counter_page` (`id`, `page`) VALUES
	(2, '83d2bf80-e87f-5262-a71e-7bdb31a971bb'),
	(3, '5661e6a7-d527-5266-91e4-269b20a9f70d'),
	(4, '21542a81-051e-5c4f-917a-4fa3ee7b65a0');
/*!40000 ALTER TABLE `kryptonit3_counter_page` ENABLE KEYS */;

-- tablo yapısı dökülüyor forlaravel.kryptonit3_counter_page_visitor
CREATE TABLE IF NOT EXISTS `kryptonit3_counter_page_visitor` (
  `page_id` bigint(20) unsigned NOT NULL,
  `visitor_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `kryptonit3_counter_page_visitor_page_id_index` (`page_id`),
  KEY `kryptonit3_counter_page_visitor_visitor_id_index` (`visitor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- forlaravel.kryptonit3_counter_page_visitor: 4 rows tablosu için veriler indiriliyor
DELETE FROM `kryptonit3_counter_page_visitor`;
/*!40000 ALTER TABLE `kryptonit3_counter_page_visitor` DISABLE KEYS */;
INSERT INTO `kryptonit3_counter_page_visitor` (`page_id`, `visitor_id`, `created_at`) VALUES
	(2, 4, '2018-05-11 16:33:42'),
	(2, 3, '2018-05-20 20:31:42'),
	(3, 3, '2018-05-15 23:50:09'),
	(4, 3, '2018-05-20 20:32:18');
/*!40000 ALTER TABLE `kryptonit3_counter_page_visitor` ENABLE KEYS */;

-- tablo yapısı dökülüyor forlaravel.kryptonit3_counter_visitor
CREATE TABLE IF NOT EXISTS `kryptonit3_counter_visitor` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `visitor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kryptonit3_counter_visitor_visitor_unique` (`visitor`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- forlaravel.kryptonit3_counter_visitor: 2 rows tablosu için veriler indiriliyor
DELETE FROM `kryptonit3_counter_visitor`;
/*!40000 ALTER TABLE `kryptonit3_counter_visitor` DISABLE KEYS */;
INSERT INTO `kryptonit3_counter_visitor` (`id`, `visitor`) VALUES
	(4, 'ba36b5c8efd853c7f40cdf1ec9e107a0fe5310df726e3b45c7a67592cd4f13f3'),
	(3, '0185711aa83048200111cae9f2ac4decaf56cbe0c272e73ff1e2f3fa6eb75bd9');
/*!40000 ALTER TABLE `kryptonit3_counter_visitor` ENABLE KEYS */;

-- tablo yapısı dökülüyor forlaravel.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- forlaravel.migrations: ~12 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(4, '2014_10_12_000000_create_users_table', 1),
	(5, '2014_10_12_100000_create_password_resets_table', 1),
	(6, '2018_05_04_085556_create_admin_table', 1),
	(7, '2018_05_07_095658_orders_create_table', 2),
	(8, '2018_05_07_100007_orders_status_table', 2),
	(9, '2018_05_07_100105_contact_table', 2),
	(10, '2018_05_07_100142_pages_table', 2),
	(11, '2015_06_21_181359_create_kryptonit3_counter_page_table', 3),
	(12, '2015_06_21_193003_create_kryptonit3_counter_visitor_table', 3),
	(13, '2015_06_21_193059_create_kryptonit3_counter_page_visitor_table', 3),
	(14, '2018_05_11_004733_create_blog_table', 4),
	(15, '2018_05_11_005243_create_companies_table', 5),
	(16, '2018_05_11_005736_create_companies_detail_table', 5),
	(17, '2018_05_11_010209_create_blog_categories_table', 5),
	(18, '2018_05_15_215848_create_abouts_table', 6);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- tablo yapısı dökülüyor forlaravel.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alias` char(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `editor_id` int(10) unsigned NOT NULL,
  `image` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- forlaravel.pages: ~3 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `pages`;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` (`id`, `alias`, `title`, `content`, `description`, `keywords`, `slug`, `editor_id`, `image`, `created_at`, `updated_at`) VALUES
	(1, 'corporate', 'About Us', 'about us page content area. asd', 'about,us asd', 'about,us, qwe', 'about-us', 1, '1.jpg', '2018-04-28 15:23:15', '2018-05-07 16:13:10'),
	(2, 'corporate', 'About Company', 'about company page content area.', 'about,us', 'about,us', 'about-company', 1, '2.png', '2018-04-28 15:23:15', '2018-04-28 15:23:15'),
	(3, 'corporate', 'test', 'test', 'test', 'test', 'test', 1, '3.jpg', '2018-05-02 11:05:27', '2018-05-02 11:05:27');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;

-- tablo yapısı dökülüyor forlaravel.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- forlaravel.password_resets: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
	('info@siberfx.com', '$2y$10$qsWCFHRt09bQoY4dMHEm/OWgW1aAjx3LVw7BIGTw7TNpAM8KCybqq', '2018-05-05 10:58:29');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
