<?php

namespace App\Http\Controllers;
use App;
use App\About;
use Illuminate\Http\Request;

class HomeAnyController extends HomeController
{


    public function index()
    {
        return view('frontend.home');
    }

    public function corporate(Request $request, $slug) {

        $langprefix = app()->getLocale();
        $aboutus = About::where('slug_'.$langprefix, $slug)->first();

        $title = 'title_'.$langprefix;
        $content = 'content_'.$langprefix;
        $keywords = 'keyword_'.$langprefix;
        $slug = 'slug_'.$langprefix;


        $info = array(
            'title' => $aboutus->$title,
            'content' => $aboutus->$content,
            'keywords' => $aboutus->$keywords,
            'slug' => $aboutus->$slug,
        );

        return view('frontend.corporate', compact('info'));
    }
}
