<?php

namespace App\Http\Controllers;
use App\Contact;
use Request;
use View;

class HomeController extends Controller
{

    public function __construct()
    {
        $sitedata = Contact::contact();

        View::share('sitedata', $sitedata);
    }

}
