<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Pages;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManagerStatic as Image;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('backend.admin');
    }

    public function pages_list()
    {
        $pages = DB::table('pages')->where('alias','=','corporate')->get();


        return view('backend.pagelist', compact('contact', 'pages'));
    }

    public function pages_edit($id)
    {
        $pages = new Pages;
        $pages = Pages::where('id','=', $id )->where('alias','=','corporate')->first();
        return view('backend.pageedit', compact('pages'));
    }

    public function pages_save(Request $request, $id)
    {

        $slugify = new Slugify(['regexp' => '/([^A-Za-z0-9]|-)+/']);

        $image = new Image;

        $pages = Pages::findOrFail($id);

        $pages->alias = 'corporate';
        $pages->title = $request->input('title');
        $pages->content = $request->input('content');
        $pages->description = $request->input('description');
        $pages->keywords = $request->input('keywords');
        $pages->slug = $slugify->slugify($request->input('title'));

        $pages->save();

        return view('backend.pageview', compact('pages'));

    }

    public function pages_addsave(Request $request)
    {
        $slugify = new Slugify(['regexp' => '/([^A-Za-z0-9]|-)+/']);

        $pages = new Pages;

        $pages->alias = 'corporate';
        $pages->title = $request->input('title');
        $pages->content = $request->input('content');
        $pages->description = $request->input('description');
        $pages->keywords = $request->input('keywords');
        $pages->slug = $slugify->slugify($request->input('title'));
        $pages->editor_id = Auth::user()->id;

        // This is the place where we get the name of the file requested for upload, we change the name by slugify module,
        // seperating extension and filename, then calopsing while saving it to the path and to the database.
        $fileinfo   = $request->file('image')->getClientOriginalName();
        $imagename  = $slugify->slugify(pathinfo($fileinfo, PATHINFO_FILENAME));
        $imageext   = pathinfo($fileinfo, PATHINFO_EXTENSION);


        Image::make($request->file('image'))->resize(200, 200)->save('media/' . $imagename . '.' . $imageext); // First trying for image manipulation
        $pages->image = $imagename . '.' . $imageext;

        $pages->save();

        return redirect()->back();

    }



    public function pages_add()
    {

        return view('backend.pagesadd');

    }

    public function pages_delete(Request $request, $id)
    {
        $pages = Pages::findOrFail($id);
        $pages->delete();
        return redirect()->back();
    }

    public function pages_view($id)
    {

        $pages = Pages::findOrFail($id);
        return view('backend.pageview', compact('pages'));

    }

    public function contact() {

        $cdata = Contact::first();

        return view('backend.contact', compact('cdata'));
    }

    public function contact_update(Request $request, $id)
    {
        $contact = Contact::findOrFail($id);

        $contact->title      = $request->input('title');
        $contact->address    = $request->input('address');
        $contact->email      = $request->input('email');
        $contact->phone      = $request->input('phone');
        $contact->mobile     = $request->input('mobile');
        $contact->copyright  = $request->input('copyright');
        $contact->updated_at = \Carbon\Carbon::now();

        $contact->save();

        response()->json($contact);


    }
}
