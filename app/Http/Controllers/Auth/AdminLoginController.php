<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class AdminLoginController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest:admin', ['except' => ['logout']]);
    }

    public function showLoginForm ()
    {
        return view('auth.admin-login');
    }

    public function login(Request $request)
    {
        // Validating the data
        $this->validate( $request, [
           'email'      =>  'required|email',
           'password'   =>  'required|min:6'
        ]);

        // Attempt the user log in

    if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password ], $request->remember))
    {
        // If succeed, redirect to dashboard
        return redirect()->intended(route('admin.dashboard'));
    }
        // unsucceed then redirect back to login with form data. ( email and remember will be remembered).
        return redirect()->back()->withInput($request->only('email','remember'));
    }

    public function logout()
    {
        Auth::guard('admin')->logout();

        return redirect('/admin');
    }
}
