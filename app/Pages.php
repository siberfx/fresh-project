<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $table = 'pages';

    protected $fillable = [
        'alias',
        'title',
        'content',
        'description',
        'keywords',
        'slug',
    ];
}
