<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $table = 'about';

    protected $fillable = [
        'title_tr',
        'title_en',
        'title_fr',
        'content_tr',
        'content_en',
        'content_fr',
        'keyword_tr',
        'keyword_en',
        'keyword_fr',
        'slug_tr',
        'slug_en',
        'slug_fr',
    ];
}
