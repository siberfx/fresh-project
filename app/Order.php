<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'status',
        'title',
        'content',
        'editor_id',
        'date_issue',
    ];
}

// public function orderstatus ()
// {
// return $this->belongsTo(OrderStatus::class);
// }