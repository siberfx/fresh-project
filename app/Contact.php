<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    private static $_data;

    protected $table = 'contact';

    protected $fillable = [
        'title',
        'address',
        'email',
        'phone',
        'mobile',
        'copyright',
    ];

    public static function contact()
    {
        if (!self::$_data) {
            self::$_data = static::find(1);
        }
        return self::$_data;
    }
}
