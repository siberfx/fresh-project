		var LoadedLazy = false,
		    cursize = 0;

		$(document).ready(function() {
        $('.md-trigger').on('click', function() {
            $('.md-modal').addClass('md-show');
            setTimeout(function() {
                $('.arabul').focus();
            }, 500)
        });
		
        $('.md-close').on('click', function() {
            $('.md-modal').removeClass('md-show');
        });

		    if ($("img.loadAfter").length) {
		        $("body").mousemove(function() {
		            if (LoadedLazy == false) {
		                $("img.loadAfter").each(function(index) {
		                    $(this).attr("src", $(this).attr("data-original"));
		                });
		                $("body").unbind("mousemove");
		                LoadedLazy = true;
		            }
		        });
		        if ($(window).width() < 767) {
		            console.log("Mobil View")
		            $("img.loadAfter").each(function(index) {
		                $(this).attr("src", $(this).attr("data-original"));
		            });
		        }
		    }

		    $('#incfont').click(function() {
		        curSize = parseInt($('.fontsizer, .fontsizer p').css('font-size')) + 2;
		        if (curSize <= 20) $('.fontsizer, .fontsizer p').css('font-size', curSize);
		    });
		    $('#decfont').click(function() {
		        curSize = parseInt($('.fontsizer, .fontsizer p').css('font-size')) - 2;
		        if (curSize >= 12) $('.fontsizer, .fontsizer p').css('font-size', curSize);
		    });

		    if ($(".SocialButtonList").length) {
		        var a = $(location).attr("href");
		        $("a.SocHref").each(function(b, c) {
		            var d = $(this).attr("href"),
		                e = d + a;
		            $(this).attr("href", e)
		        })
		    }

$('#fullpage').fullpage({
    anchors: ['bolum0', 'bolum1', 'bolum2', 'bolum3','bolum4', 'bolum5', 'bolum6'],
    navigation: true,
    menu: '#menu',
    navigationPosition: 'right',
    navigationTooltips: [,'Ertok Yapı Malzemeleri', 'Ertok Sigorta', 'Yemenoğulları Hırdavat', 'Yemenoğlu Gayrimenkul',  'Öztaş Gayrimenkul', 'Ges Madencilik'],
    css3: true
});



		    $("#owl-eyapi").owlCarousel({
		        autoplay: 500, //Set AutoPlay to 3 seconds
		        loop: true,
		        nav: true,
		        items: 1,
		        itemsDesktop: [1199, 1],
		        itemsDesktopSmall: [979, 1]
		    });

		    $("#owl-esig").owlCarousel({
		        autoplay: 500, //Set AutoPlay to 3 seconds
		        loop: true,
		        nav: true,
		        items: 1,
		        itemsDesktop: [1199, 1],
		        itemsDesktopSmall: [979, 1]
		    });

		    $("#owl-yhir").owlCarousel({
		        autoplay: 500, //Set AutoPlay to 3 seconds
		        loop: true,
		        nav: true,
		        items: 1,
		        itemsDesktop: [1199, 1],
		        itemsDesktopSmall: [979, 1]
		    });

		    $("#owl-ygay").owlCarousel({
		        autoplay: 500, //Set AutoPlay to 3 seconds
		        loop: true,
		        nav: true,
		        items: 1,
		        itemsDesktop: [1199, 1],
		        itemsDesktopSmall: [979, 1]
		    });

		    $("#owl-ozt").owlCarousel({
		        autoplay: 500, //Set AutoPlay to 3 seconds
		        loop: true,
		        nav: true,
		        items: 1,
		        itemsDesktop: [1199, 1],
		        itemsDesktopSmall: [979, 1]
		    });

		    $("#owl-gesm").owlCarousel({
		        autoplay: 500, //Set AutoPlay to 3 seconds
		        loop: true,
		        nav: true,
		        items: 1,
		        itemsDesktop: [1199, 1],
		        itemsDesktopSmall: [979, 1]
		    });


		    var $item = $('.carousel.sbxmain .item');
		    var $wHeight = $(window).height();

				    $item.eq(0).addClass('active');
				    $item.height($wHeight);
				    $item.addClass('full-screen');

				    $('.carousel.sbxmain .item img').each(function() {
				        var $src = $(this).attr('src');
				        var $color = $(this).attr('data-color');
				        $(this).parent().css({
				            'background-image': 'url(' + $src + ')',
				            'background-color': $color
				        });
				        $(this).remove();
				    });


				    $(window).on('resize', function() {
				        $wHeight = $(window).height();
				        $item.height($wHeight);
				    });
				    $('.carousel.sbxmain').carousel({
				        interval: 6000,
				        pause: "false"
				    });
 
			if ($('.unclick').length) {
				$(".unclick").click(function(e) {
					event.preventDefault();
				});
			}

		});


$('img').each(function(){

$(this).attr('onerror', 'this.src="/media/noimg.jpg"');

});