<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ Config::get('app.name') }}</title>
    <!-- Styles -->
    <link rel="dns-prefetch" href="https://code.jquery.com">
    <link rel="dns-prefetch" href="https://fonts.googleapis.com">
    <link rel="dns-prefetch" href="https://cdnjs.cloudflare.com">
    <link rel="dns-prefetch" href="https://stackpath.bootstrapcdn.com">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Rajdhani:300,400,700&subset=latin-ext" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/main.css') }}">

</head>
<body>
    <div id="app">

        <nav class="navbar navbar-expand-md navbar-light bg-light fixed-top">
            <a class="navbar-brand" href="#"><img src="http://cdn.siberfx.com/media/logo/logo.svg" /></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse flex-column ml-lg-0" id="navbarsExampleDefault">

                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown mr-2 language">
                        <a class="nav-link dropdown-toggle" href="#" id="dd06" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-globe"></i> TR</a>
                        <div class="dropdown-menu" aria-labelledby="dd06">
                            <a class="dropdown-item" href="#"><i class="fa fa-check"></i> TR</a>
                            <a class="dropdown-item" href="#"><i class="fa fa-globe"></i> EN</a>
                            <a class="dropdown-item" href="#"><i class="fa fa-globe"></i> FR</a>
                            <a class="dropdown-item" href="#"><i class="fa fa-globe"></i> RU</a>
                        </div>
                    </li>


                    @if (Auth::guest())
                        <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('register') }}">Register</a></li>
                    @else
                        <li class="nav-item dropdown mr-2">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</a>
                            <div class="dropdown-menu" aria-labelledby="dropdown01">
                                <a class="nav-link" href="{{ route('admin.login') }}">
                                    <i class="fa fa-cogs"></i> User Panel
                                </a>
                                <a class="nav-link" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out"></i> Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>

                    @endif



                    <form name="search-in" method="POST" action="{{ url('/search') }}" class="form-inline my-2 my-lg-0 mr-5">
                        <input name="search-in" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </ul>

                <ul class="navbar-nav ml-auto mr-5 flex-row">

                    <li class="nav-item active">
                        <a class="nav-link" href="#"><i class="fa fa-home"></i> mainpage <span class="sr-only"></span></a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dd01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">corporate</a>
                        <div class="dropdown-menu" aria-labelledby="dd01">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dd02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">orders</a>
                        <div class="dropdown-menu" aria-labelledby="dd02">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dd03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">contact</a>
                        <div class="dropdown-menu" aria-labelledby="dd03">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>



                </ul>

            </div>
        </nav>



        @yield('content')
    </div>

    <footer class="footer">
        <div class="container">
            <span class="text-muted">
                @yield('copyright')
            </span>
        </div>
    </footer>

    <!-- Scripts -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="//stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="{{asset ('js/main.js') }}"></script>
    @yield('exjquery')
</body>
</html>
