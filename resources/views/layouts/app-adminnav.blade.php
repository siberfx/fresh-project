<nav class="navbar navbar-expand-md navbar-light bg-light fixed-top">
    <a class="navbar-brand" href="#"><img src="http://cdn.siberfx.com/media/logo/logo.svg" /></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse flex-column ml-lg-0" id="navbarsExampleDefault">

        <ul class="navbar-nav ml-auto">


                <li class="nav-item dropdown mr-2">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> {{ Auth::user()->name }}</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="nav-link" href="{{ url('/') }}">
                            <i class="fa fa-cogs"></i> Frontend
                        </a>
                        <a class="nav-link" href="{{ url('/admin/profile/') }}">
                            <i class="fa fa-cogs"></i> Profile
                        </a>
                        <a class="nav-link" href="{{ url('/admin/logout') }}"
                           onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i> Logout
                        </a>
                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>



        </ul>

        <ul class="navbar-nav mr-auto ml-5">

            <li class="nav-item">
                <a class="nav-link" href="{{ url('/admin') }}">Admin Dashboard</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link" href="{{ url('/admin/pagelist') }}">Dynamic Pages</a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dd02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">orders</a>
                <div class="dropdown-menu" aria-labelledby="dd02">
                    <a class="dropdown-item" href="#">List All</a>
                    <a class="dropdown-item" href="#">List Re-opened</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dd03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">settings</a>
                <div class="dropdown-menu" aria-labelledby="dd03">
                    <a class="dropdown-item" href="{{ url('/admin/contact') }}">Site Configurations</a>
                    <a class="dropdown-item" href="#">Language Configurations</a>
                    <a class="dropdown-item" href="#">Admin Configurations</a>
                </div>
            </li>




        </ul>

    </div>
</nav>
