<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <link rel="dns-prefetch" href="//code.jquery.com">
    <link rel="dns-prefetch" href="//fonts.googleapis.com">
    <link rel="dns-prefetch" href="//cdnjs.cloudflare.com">
    <link rel="dns-prefetch" href="//stackpath.bootstrapcdn.com">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="/media/logo/favicon.png" type="image/png" sizes="16x16">

    <title>@yield('title')</title>

    @yield('css')

</head>
    <div id="header">

        <nav class="navbar navbar-expand-md navbar-light bg-light fixed-top">
            <div class="container">
            <a class="navbar-brand" href="{{ url(app()->getLocale()) }}"><img src="/media/logo/logo.png" /></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse flex-column ml-lg-0" id="navbarsExampleDefault">
                <ul class="navbar-nav ml-auto mr-5">
                    <li class="nav-item dropdown mr-2 language">
                        <a class="nav-link dropdown-toggle" href="#" id="dd06" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-globe"></i> {{ app()->getLocale()  }}</a>
                        <div class="dropdown-menu" aria-labelledby="dd06">
                            <a class="dropdown-item" href="/tr"><i class="fa fa-globe"></i> TR</a>
                            <a class="dropdown-item" href="/en"><i class="fa fa-globe"></i> EN</a>
                            <a class="dropdown-item" href="/fr"><i class="fa fa-globe"></i> FR</a>
                        </div>
                    </li>



                    <form name="search-in" method="POST" action="search" class="form-inline my-2 my-lg-0">
                        <input name="search-in" class="form-control mr-sm-2" type="search" placeholder="Ara" aria-label="Ara">
                        <button class="btn btn-danger my-2 my-sm-0" type="submit">Ara</button>
                    </form>
                </ul>


                <ul class="navbar-nav ml-auto mr-5">


                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dd00" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ __('links.corporate_nav') }}</a>
                        <div class="dropdown-menu" aria-labelledby="dd00">
                            <a class="dropdown-item" href="{{ url('/tr/grup-sirketler/baslik') }}">Hakkımızda</a>
                            <a class="dropdown-item" href="{{ url('/tr/grup-sirketler/baslik') }}">Vizyonumuz</a>
                            <a class="dropdown-item" href="{{ url('/tr/grup-sirketler/baslik') }}">Misyonumuz</a>
                            <a class="dropdown-item" href="{{ url('/tr/grup-sirketler/baslik') }}">Tarihçe</a>
                        </div>
                    </li>


                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dd01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ __('links.group_nav') }}</a>
                        <div class="dropdown-menu" aria-labelledby="dd01">
                            <a class="dropdown-item" href="#bolum1">Ertok Yapı Malzemeleri</a>
                            <a class="dropdown-item" href="#bolum2">Yemenoğulları Hırdavat</a>
                            <a class="dropdown-item" href="#bolum3">Yemenoğlu Gayrimenkul</a>
                            <a class="dropdown-item" href="#bolum4">Öztaş Gayrimenkul</a>
                            <a class="dropdown-item" href="#bolum5">Ges Madencilik</a>
                            <a class="dropdown-item" href="#bolum6">Ertok Sigorta</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dd02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ __('links.media_nav') }}</a>
                        <div class="dropdown-menu" aria-labelledby="dd02">
                            <a class="dropdown-item" href="{{ url('/tr/grup-sirketler/baslik') }}">Haberler</a>
                            <a class="dropdown-item" href="{{ url('/tr/grup-sirketler/baslik') }}">Tanıtım Filmi</a>
                            <a class="dropdown-item" href="{{ url('/tr/grup-sirketler/baslik') }}">Kurumsal Logolar</a>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/tr/grup-sirketler/baslik') }}"> {{ __('links.hr_nav') }} <span class="sr-only"></span></a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dd03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ __('links.contact_nav') }}</a>
                        <div class="dropdown-menu" aria-labelledby="dd03">
                            <a class="dropdown-item" href="{{ url('/tr/grup-sirketler/baslik') }}">Adresimiz</a>
                            <a class="dropdown-item" href="{{ url('/tr/grup-sirketler/baslik') }}">Bize Ulaşın</a>
                        </div>
                    </li>



                </ul>

            </div>
        </div>
        </nav>



    </div>
@yield('content')
@yield('js')
</html>