@extends('layouts.app-admin')
@section('title')
    {{  $contact->title }}
@stop
@section('copyright')
    {{  $contact->copyright }}
@stop
@section('content')
    <div class="container sbx-starter">
        <div class="row">
            <div>
                <a href="{{ url('/admin') }}" class="btn btn-danger mb-4"><i class="fa fa-arrow-left"></i> Back </a>
                <h2>List of dynamic pages</h2>
            </div>
            <table class="table table-hover table-bordered table-responsive-sm">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pages as $page)
                    <tr>
                        <td>{{$page->title}}</td>
                        <td>{{$page->content}}</td>
                        <td>
                            <a class="btn btn-sm btn-success" href="{{ url('admin/page_view/' . $page->id) }}"><i class="fa fa-eye"></i> Preview</a>
                            <a class="btn btn-sm btn-primary" href="{{ url('admin/page_edit/' . $page->id) }}"><i class="fa fa-edit"></i> Edit</a>
                            <a onclick="return confirm('Are you sure you want to remove this page?');" class="btn btn-sm btn-danger" href="{{ url('/admin/page_delete/' . $page->id) }}"><i class="fa fa-trash"></i> Delete</a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>

    </div>
@endsection
@section('exjquery')

@stop