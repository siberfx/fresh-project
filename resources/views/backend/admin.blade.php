@extends('layouts.app-admin')
@section('title')
    {{  $contact->title }}
@stop
@section('copyright')
    {{  $contact->copyright }}
@stop
@section('content')
<div class="container sbx-starter">
    <div class="row">
        <div class="col-md-8 ml-auto mr-auto">
            <div class="card card-default">
                <div class="card-header">Admin Dash</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('exjquery')

@stop