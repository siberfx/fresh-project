@extends('layouts.app-admin')
@section('title')
    {{  $contact->title }}
@stop
@section('copyright')
    {{  $contact->copyright }}
@stop

@section('content')
    <div class="container sbx-starter">
        <div class="row">
            <div class="col-md-10">

                <form id="contactForm" action="{{'/admin/update_contact/' . $cdata->id}}" method="POST">

                    <div class="form-group">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}" />
                        <label>Site Title</label>
                        <input name="title" type="text" class="form-control" aria-describedby="textHelp" placeholder="Enter title" value="{{$cdata->title}}">
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Phone</label>
                                <input name="phone" type="text" class="form-control" aria-describedby="textHelp" placeholder="Enter title" value="{{$cdata->phone}}">
                            </div>
                            <div class="col-md-6">
                                <label>Mobile Phone</label>

                                <input name="mobile" type="text" class="form-control" aria-describedby="textHelp" placeholder="Enter title" value="{{$cdata->mobile}}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>E-mail</label>
                                <input name="email" type="text" class="form-control" aria-describedby="textHelp" placeholder="Enter e-mail address" value="{{$cdata->email}}">
                            </div>
                            <div class="col-md-6">
                                <label>E-mail 2</label>

                                <input name="email2" type="text" class="form-control" aria-describedby="textHelp" placeholder="Enter second e-mail address" value="{{$cdata->email2}}">
                            </div>
                        </div>
                    </div>



                    <div class="form-group">
                        <label>Address</label>
                        <textarea name="address" class="form-control" aria-describedby="addrHelp" placeholder="Description of issue">{{$cdata->address}}</textarea>
                        <small id="addrHelp" class="form-text text-muted">Please enter your full address for the site.</small>
                    </div>

                    <div class="form-group">
                        <label>Copyright Text</label>
                        <input name="copyright" type="text" class="form-control" placeholder="Format : 12/01/2018" value="{{$cdata->copyright}}">
                    </div>

                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                    <a href="{{ url('admin') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back </a>

                </form>


            </div>
        </div>
    </div>

@endsection

@section('exjquery')
    <script>
        // Update posted data
        $(document).on('submit', '#contactForm', function(e) {
            e.preventDefault();

            var datastring = $("#contactForm").serialize();

            $.ajax({
                type: 'POST',
                url: $('#contactForm').attr('action'),
                data: datastring,
                success: function(data) {
                    $('.errorTitle').addClass('hidden');
                    $('.errorContent').addClass('hidden');

                    if ((data.errors)) {
                        setTimeout(function () {
                            toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                        }, 500);

                        if (data.errors.title) {
                            $('.errorTitle').removeClass('hidden');
                            $('.errorTitle').text(data.errors.title);
                        }
                        if (data.errors.content) {
                            $('.errorContent').removeClass('hidden');
                            $('.errorContent').text(data.errors.content);
                        }
                    } else {
                        toastr.success('Successfully updated Post!', 'Success Alert', {timeOut: 5000});


                    }
                }
            });
        });
    </script>
@stop

