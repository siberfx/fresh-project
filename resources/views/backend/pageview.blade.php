@extends('layouts.app-admin')
@section('title')
    {{  $contact->title }}
@stop
@section('copyright')
    {{  $contact->copyright }}
@stop
@section('content')
    <div class="container sbx-starter">

            <div class="card col-12">
                <a href="{{ url('admin/pagelist') }}" class="btn btn-danger"> <i class="fa fa-arrow-left"></i> Back </a>
                <div class="card-header">Preview for the page content</div>
            <div class="card-body">
                <ul class="list-group">
                    <li class="list-group-item"><span class="text-info">Title:</span> {{$pages->title}}</li>
                    <li class="list-group-item"><span class="text-info">Publish date:</span> {{ $pages->created_at}}<br /></li>
                    <li class="list-group-item"><span class="text-info">Description:</span> {{$pages->description}}<br /></li>
                    <li class="list-group-item"><span class="text-info">Keywords:</span> {{$pages->keywords}}</li>
                    <li class="list-group-item"><span class="text-info"></span> <img width="150" src="{{ url('media/'.$pages->image) }}" /></li>
                    <br />
                    <div>
                    <a class="btn btn-primary" href="{{ url('admin/page_edit/' . $pages->id) }}">Edit</a>
                    <a onclick="return confirm('Are you sure you want to close this page?');" class="btn btn-danger" href="{{ url('admin/page_delete/' . $pages->oid) }}">Delete</a></li>
                    </div>
                </ul>

            </div>
            <div class="card-footer">Made by : Selim GORMUS || </div>
            </div>
    </div>
@endsection
@section('exjquery')

@stop