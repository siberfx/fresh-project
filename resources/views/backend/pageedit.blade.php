@extends('layouts.app-admin')
@section('title')
    {{  $contact->title }}
@stop
@section('copyright')
    {{  $contact->copyright }}
@stop
@section('content')
    <div class="container sbx-starter">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <form name="pagesForm" action="{{ url('/admin/page_save') .'/'. $pages->id}}" method="post" enctype="multipart/form-data">


                    <div class="form-group">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}" />
                        <label>Title</label>
                        <input name="title" type="text" class="form-control" aria-describedby="textHelp" placeholder="Enter title" value="{{$pages->title}}" >
                        <small id="textHelp" class="form-text text-muted">This is the title of the page can not be empty!</small>
                    </div>

                    <div class="form-group">
                        <label>Content</label>
                        <textarea name="content" class="form-control" aria-describedby="descHelp" placeholder="Content">{{$pages->content}}</textarea>
                        <small id="descHelp" class="form-text text-muted">Content of the page.</small>
                    </div>

                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="description" class="form-control" aria-describedby="descHelp" placeholder="Content">{{$pages->description}}</textarea>
                        <small id="descHelp" class="form-text text-muted">Short description of the page.</small>
                    </div>

                    <div class="form-group">
                        <label>Keywords</label>
                        <input name="keywords" type="text" class="form-control" aria-describedby="textHelp" placeholder="Enter title" value="{{$pages->keywords}}" >
                    </div>

                    <div class="form-group">
                        @if( $pages->image != "")
                            <img width="100" src="{{ url('/media/' . $pages->image ) }}" />
                            <a class="alert-link" href="{{ url('corporate/remove_image/'. $pages->id) }}"> Remove / Replace</a>
                        @else()
                            <label>Image </label>
                            <input type="file" name="image" />

                        @endif
                    </div>

                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                    <a href="{{ url('/admin/pagelist') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back </a>

                </form>


            </div>
        </div>
    </div>
@endsection
@section('exjquery')

@stop