@extends('layouts.app')

@section('title')
{{ app()->getLocale() . ' - '. $sitedata->title }}
@stop

@section('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Rajdhani:300,400,700&subset=latin-ext" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/helper.css" rel="stylesheet">
    <link href="/css/owl.carousel.css" rel="stylesheet">
    <link href="/css/owl.theme.default.css" rel="stylesheet">
    <link href="/css/jquery.fullPage.css" rel="stylesheet">
@stop


@section('copyright')
{{ $sitedata->copyright }}
@stop

<body id="homepage">
<div id="socialarea" class="hidden-sm-up">
  <ul id="sociallist">
    <li class="facebook_social"> <a href="http://www.facebook.com/" target="_new"><i class="fa fa-facebook"></i></a> </li>
    <li class="twitter_social"> <a href="https://twitter.com/" target="_new"><i class="fa fa-twitter"></i></a> </li>
    <li class="googleplus_social"> <a href="https://plus.google.com/" target="_new"><i class="fa fa-google-plus"></i></a> </li>
    <li class="linkedin_social"> <a href="http://www.linkedin.com/" target="_new"><i class="fa fa-linkedin"></i></a> </li>
    <li class="pinterest_social"> <a href="http://pinterest.com/" target="_new"><i class="fa fa-pinterest-p"></i></a> </li>
    <li class="instagram_social"> <a href="https://www.instagram.com//" target="_new"><i class="fa fa-instagram"></i></a> </li>
  </ul>

</div>

@section('content')


    {{ Counter::Count('home') }}

    <div id="fullpage">
        <div class="section " id="section">

        <!-- Ertok Yapı Malzemeleri -->
            <div id="mycarousel" class="carousel slide sbxmain" data-ride="carousel">
               <div class="carousel-inner" role="listbox">
                  <div class="carousel-item active">
                     <img src="/media/slide/s1.jpg" alt="">
                     <div class="carousel-caption">
                        <h1 class="slider mobilslider">Çevrecilik Anlayışını</h1>
                        <h2 class="slider mobilslider">Yenilik ve Modernlik İle Harmanlıyoruz.</h2>
                        <a href="#" class="btn btnincele text-center">İncele</a>
                     </div>
                  </div>
                  <div class="carousel-item">
                     <img src="/media/slide/s2.jpg" alt="">
                     <div class="carousel-caption">
                        <h1 class="slider mobilslider">Gelecek Nesillere</h1>
                        <h2 class="slider mobilslider">Yaşanılabilir Bir Çevre Bırakabilmek Amacıyla Çalışıyoruz</h2>
                        <a href="#" class="btn btnincele text-center">İncele</a>
                     </div>
                  </div>
                  <div class="carousel-item">
                     <img src="/media/slide/s3.png" alt="">
                     <div class="carousel-caption">
                        <h1 class="slider mobilslider">Doğaya Dost</h1>
                        <h2 class="slider mobilslider">Uygulamalarımızla Doğal Kaynakların Sürdürülebilirliğini Hedefliyoruz.</h2>
                        <a href="#" class="btn btnincele text-center">İncele</a>
                     </div>
                  </div>
                  <div class="carousel-item">
                     <img src="/media/slide/s4.jpg" alt="">
                     <div class="carousel-caption">
                        <h1 class="slider mobilslider">Sosyal Sorumluluk</h1>
                        <h2 class="slider mobilslider">Projelerimizle Toplumsal Bilincin Oluşturulması İçin Çabalıyoruz.</h2>
                        <a href="#" class="btn btnincele text-center">İncele</a>
                     </div>
                  </div>

              </div>
               <a class="carousel-control-prev" href="#mycarousel" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Önceki</span>
               </a>
               <a class="carousel-control-next" href="#mycarousel" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Sonraki</span>
               </a>
            </div>
        </div>
        <!-- Yapı malzemeleri -->
        <div class="section" id="section1">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 ">
                        <div id="owl-eyapi" class="owl-carousel owl-theme">
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s4.jpg" alt="Ertok Yapı Malzemeleri" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s4.jpg" alt="Ertok Yapı Malzemeleri" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s4.jpg" alt="Ertok Yapı Malzemeleri" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s4.jpg" alt="Ertok Yapı Malzemeleri" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s4.jpg" alt="Ertok Yapı Malzemeleri" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s4.jpg" alt="Ertok Yapı Malzemeleri" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s4.jpg" alt="Ertok Yapı Malzemeleri" /></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="boxed">
                            <img class="loadAfter" data-original="/media/logo/logo.png" class="img-responsive center-block" alt="Ertok Yapı Malzemeleri" />
<br /><br />
                            <p class="yazimetin">ERTOK Yapı Malzemeleri San Tic. A.Ş. 2000 yılında İstanbul’da kurulmuş olup inşaat sektörünün başta hırdavat olmak üzere yapı kimyasalları, izolasyon, iş güvenliği malzemeleri, elektrik, mekanik, tesisat ve kaynak malzemeleri gibi birçok alanda adrese teslim tedarik yapan, sektörün öncü firmalarından birisidir.

                                Firmamız faaliyetlerini İstanbul Çekmeköy’deki merkez binasında sürdürmekle beraber, Yurtdışında Azerbaycan Bakü’de de bir yerleşkesi bulunmaktadır.

                                Ertok, yapı malzemeleri sektöründeki tecrübesi, dinamik, deneyimli ve güler yüzlü çalışanları ile   kaliteden ödün vermeden, hızlı, güvenilir ve ekonomik bir şekilde, siz değerli müşterilerinin hizmetindedir.

                                Kurulduğu tarihten itibaren şantiyelerin ihtiyaçları doğrultusunda, müşteri memnuniyetini baz alarak ERTOK, inşaat-mühendislik firmalarının çözüm ortağıdır. </p>
                            <a href="#" class="btn btnstd"><i class="fa fa-globe"></i> Ertok Yapı Malzemeleri</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Sigorta -->

        <div class="section" id="section2">


            <div class="container">
                <div class="row">
                    <div class="col-md-5 " style="margin-top:100px;">
                        <div id="owl-yhir" class="owl-carousel owl-theme">
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Yemenoğulları Hırdavat" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Yemenoğulları Hırdavat" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Yemenoğulları Hırdavat" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Yemenoğulları Hırdavat" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Yemenoğulları Hırdavat" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Yemenoğulları Hırdavat" /></div>
                        </div>
                    </div>
                    <div class="col-md-7"><br><br>
                        <div class="boxed">
                            <img class="loadAfter" data-original="/media/logo/logo.png" class="img-responsive" alt="Yemenoğulları Hırdavat" />
<br /><br />
                            <p class="yazimetin">Bir ERTOK iştiraki olan Yemenoğulları Hırdavat firması 2009 yılında İstanbul’da kurulmuştur.<br />
                                Tüm faaliyetlerini yapı inşaat ve mühendislik sektöründeki temel ve teknik ihtiyaçların tedarikinin ihracatını yapmak üzere odaklanmış vizyoner bir firmadır.
                                Ana ürün grupları teknik hırdavat, inşaat malzemeleri, yapı kimyasalları, izolasyon, iş güvenliği malzemeleri, elektrik, mekanik, tesisat ve kaynak malzemeleri, ısıtma soğutma, yangın söndürme ürünleridir.<br />
                                2015 yılında Türkiye’nin en çok ihracat yapan ilk 1000 firması içine girmiş olup, aynı zamanda uluslararası birçok inşaat ve mühendislik firmasının resmi tedarikçilerinden biridir.<br />
                                Yurtdışında başta Orta Asya ülkeleri olmak üzere Afrika ve Körfez bölgesinde 20’den fazla ülkeye yoğun bir şekilde ihracat yapmaktadır.<br />
                                Yemenoğulları Hırdavat’ın en önemli misyonu ve amacı, ülkemizin ihracatının artmasına katkıda bulunup, en iyi şekilde temsil etmek, “Made in Turkey – İyi kalite” algısının hızlı bir şekilde ihraç pazarlarımızda yerleşmesini sağlamaktır.
                            </p>
                            <a href="#" class="btn btnstd"><i class="fa fa-globe"></i> Yemenoğulları Hırdavat</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Hırdavat -->
        <div class="section" id="section3">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <br><br>
                        <div class="boxed">
                            <img class="loadAfter" data-original="/media/logo/logo.png" class="img-responsive center-block" alt="Yemenoğlu Gayrimenkul" />
<br /><br />
                            <p class="yazimetin">
                                Bir Ertok iştiraki olan Yemenoğlu Gayrimenkul 2014 yılında İstanbul’da kurulmuştur.<br />
                                Yapı inşaat malzemeleri tedariğindeki 20 yıllık tecrübesini inşaat ve gayrimenkul alanında lüks konut ve işyeri gibi prestijli projelerle gösteren İstanbul’un hızla büyüyen bir inşaat firmasıdır.<br />
                                Kuruluş felsefesi insana ve doğaya saygılı modern yaşam alanları oluşturmak, bireylerin hayatını kolaylaştıran ve yaşam kalitesini artıran mimari ve teknoloji ile buluşturmak olan Yemenoğulları Gayrimenkul, yenilikçi, akılcı, ilkeli ve sorumlu tutumuyla doğru projeler ve garantili yatırım fırsatı sunarak, MARKA firma olma yolunda emin adımlarla ilerlemektedir.
                                <br />
                                Projelerimiz:<br />
                                Botanica İstanbul, Esenyurt<br />
                                Başak Apartmanı, Bağdat Cad.<br />
                                Akros İstanbul, Beylikdüzü<br />
                                Panaroma Çamlıca, İstanbul<br />
                                Ahenk Apartmanı, Bağdat Cad.<br />
                                Reşadiye Villaları, Alemdağ, Çekmeköy<br />
                                Form Füzyon Kolejleri Eğitim Kampüsü, Sultangazi, İstanbul
                            </p>
                            <a href="#" class="btn btnstd"><i class="fa fa-globe"></i> Yemenoğlu Gayrimenkul</a>
                        </div>
                    </div>
                    <div class="col-md-5 " style="margin-top:100px;">
                        <div id="owl-ygay" class="owl-carousel owl-theme">
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Yemenoğlu Gayrimenkul" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Yemenoğlu Gayrimenkul" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Yemenoğlu Gayrimenkul" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Yemenoğlu Gayrimenkul" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Yemenoğlu Gayrimenkul" /></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- Yemenoğlu gayrimenkul -->
        <div class="section" id="section4">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <br><br>
                        <div class="boxed">
                            <img class="loadAfter" data-original="/media/logo/logo.png" class="img-responsive center-block" alt="Öztaş Gayrimenkul" />
<br /><br />
                            <p class="yazimetin">Öztaş Gayrimenkul 2014 yılında İstanbul’da kurulmuştur.<br />
Faaliyet alanı, uzun vadeli kiralama yöntemiyle, resmi tüzel ve özel kişilere ait arsalar üzerine okul kampüsleri, turizm, sanayi , konut, site, villa amaçlı müteahhitlik taşeronluk taahhüt gibi malzemeli malzemesiz inşaat yapımını gerçekleştirmektir.<br /><br />
Projelerimiz:<br />
Form Füzyon Kolejleri Eğitim Kampüsü, Sultangazi, İstanbul</p>
                            <a href="#" class="btn btnstd"><i class="fa fa-globe"></i> Öztaş Gayrimenkul</a>
                        </div>
                    </div>
                    <div class="col-md-5 " style="margin-top:100px;">
                        <div id="owl-ozt" class="owl-carousel owl-theme">
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Öztaş Gayrimenkul" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Öztaş Gayrimenkul" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Öztaş Gayrimenkul" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Öztaş Gayrimenkul" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Öztaş Gayrimenkul" /></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- öztaş -->
        <div class="section" id="section5">
            
            <div class="container">
                <div class="row">
                    <div class="boxed ml-auto mr-auto">
                    <div class="col-md-12">
                        <img data-original="/media/logo/logo.png" class="img-responsive loadAfter text-center">
<br /><br />
                        <h3 class="yazibaslik">GES DOĞALTAŞ MADEN YATIRIMLARI İNŞAAT SAN. TİC. A.Ş.</h3>
                        <p class="yazimetin">Ges  Doğaltaş Maden firması 2014 yılında İstanbul’da kurulmuştur.<br />
                        İstanbul Kurtköy’de bir taş ocağı ve Adana’da bir kum ocağı işletmesi yapmaktatır.</p>
                        <div class="col-md-4 ml-auto">
                            <a href="#" class="btn btnstd"><i class="fa fa-globe"></i> Ges Madencilik</a>
                        </div>
                    </div>
                </div>
                </div>
            </div>

        </div>


        <!-- ges madencilik -->
        <div class="section" id="section6">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 ">
                        <div id="owl-esig" class="owl-carousel owl-theme">
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Ertok Sigorta" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Ertok Sigorta" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Ertok Sigorta" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Ertok Sigorta" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Ertok Sigorta" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Ertok Sigorta" /></div>
                            <div class="item onylme"><img class="loadAfter" data-original="/media/slide/s1.jpg" alt="Ertok Sigorta" /></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="boxed">
                            <img class="loadAfter" data-original="/media/logo/logo.png" class="img-responsive center-block" alt="Ertok Sigorta" />
<br /><br />
                            <p class="yazimetin">Acenteliğimiz, ERTOK SİGORTA AR. HİZ. TİC. LTD .ŞTİ., resmi olarak 2008 yılında ülkemizin en iyi sigorta şirketlerinden UNICO SİGORTA A.Ş., HDI SİGORTA A.Ş., GULF SİGORTA A.Ş., DUBAI SİGORTA A.Ş., GÜNEŞ SİGORTA A.Ş.ve MAFRE SİGORTA A.Ş. ile sigorta sektöründe yer almaya başlamış olup sektörde birçok öncülükleri ve özellikle inşaat, nakliyat ve kasko branşlarında en kapsamlı ve en uygun fiyatlar ile adından söz ettirmektedir.
Tüm branşlarda; Sağlık Sigortaları, Yangın Sigortaları, Kaza Sigortaları, Nakliyat Sigortaları, Tekne Sigortaları, Sorumluluk Sigortaları ve Mühendislik Sigortaları başta olmak üzere, Kurumsal ve Bireysel alanda sigortalılarımızın isteklerini, ihtiyacı olduğu ölçüde doğru teminatlarla en uygun fiyatı vererek doğru poliçeye sahip olmalarını sağlamaktayız. Özellikle Nakliyat,Yangın,Kasko ve İnşaat All Risk Branşlarında ilklere imza atarak en uygun fiyatlarla kaliteli hizmet sunmaktayız. Bununla beraber hizmet anlayışımızda ilk ve en önde gelen hizmetlerden biri de yaşanan hasarlarda da sigortalılarımızın yanında olup en kısa sürede zararın karşılanması ve müşterilerimizin memnuniyetinin en üst seviyeye çıkartılmasıdır.
Başarının ve size sunduğumuz yaratıcı çözümlerin arkasındaki en önemli unsurlar; uzmanlık süreklilik, tutarlılık, size özel ve çözümler ve siz değerli müşterilerimizi iş ortağımız olarak görmemizdir. Bütün bu değerlerin size sunduğumuz hizmetin hiç bir aşamasında göz ardı edilmemesine dikkat ediyoruz.
Siz müşterilerimizle olan bütün ilişkilerimizde önceliğimiz, sizi dinleyip ihtiyaçlarınızı doğru anlayıp en doğru hizmeti sunabilmeyi temel felsefemiz olarak benimsememizdir. Bunun size verdiğimiz hizmetin kalitesini arttıracağını biliyoruz.
Risk, hayatın bir parçasıdır ve yaşadığımız her an bir risk demektir. Bizde bunu bildiğimiz için riski üzerimize nasıl alacağımızı iyi biliyoruz. Amacımız iş ortaklarımızın üstündeki bu ağır sorumluluğu  satın alarak, hayattaki sorumluluklarını azaltarak veya en aza indirerek yaşamlarını kolaylaştırmaktır.</p>
                            <a href="#" class="btn btnstd"><i class="fa fa-globe"></i> Ertok Sigorta</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

</div>


@endsection

@section('js')



    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="//stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>


    <script src="/js/jquery.backstretch.min.js"></script>
    <script src="/js/owl.carousel.js" type="text/javascript"></script>
    <script src="/js/scrolloverflow.js" type="text/javascript"></script>
    <script src="/js/jquery.fullPage.js" type="text/javascript"></script>
    <script src="/js/sbx.js"></script>
    <script>
        jQuery(document).ready(function($) {
            $("#section1").backstretch([
                "/media/bg/demo.jpg",
                "/media/bg/demo.jpg",
                "/media/bg/demo.jpg"
            ], {duration: 3000, fade: 750});
            $("#section2").backstretch([
                "/media/bg/demo.jpg",
                "/media/bg/demo.jpg",
                "/media/bg/demo.jpg"
            ], {duration: 3000, fade: 750});
            $("#section3").backstretch([
                "/media/bg/demo.jpg",
                "/media/bg/demo.jpg",
                "/media/bg/demo.jpg"
            ], {duration: 3000, fade: 750});
            $("#section4").backstretch([
                "/media/bg/demo.jpg",
                "/media/bg/demo.jpg",
                "/media/bg/demo.jpg"
            ], {duration: 3000, fade: 750});
            $("#section5").backstretch([
                "/media/bg/demo.jpg",
                "/media/bg/demo.jpg",
                "/media/bg/demo.jpg"
            ], {duration: 3000, fade: 750});
            $("#section6").backstretch([
                "/media/bg/demo.jpg",
                "/media/bg/demo.jpg",
                "/media/bg/demo.jpg"
            ], {duration: 3000, fade: 750});


        });


    </script>


@stop

    </body>