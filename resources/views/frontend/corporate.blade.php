@extends('layouts.app')
@section('title')
    {{ $info['title'] . ' - '. $sitedata->title }}
@stop

@section('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Rajdhani:300,400,700&subset=latin-ext" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/helper.css" rel="stylesheet">
    <link href="/css/owl.carousel.css" rel="stylesheet">
    <link href="/css/owl.theme.default.css" rel="stylesheet">
    <link href="/css/jquery.fullPage.css" rel="stylesheet">
@stop


@section('copyright')
    {{ $sitedata->copyright }}
@stop

<body>

    
@section('content')
<div class="container">
<nav class="mt-1 mr-0" aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">{{ __('general.mainpage') }}</a></li>
    <li class="breadcrumb-item"><a href="#">{{ __('general.coop') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ 'Baslik' }}</li>
  </ol>
</nav>
</div>
    {{ Counter::Count('corporate') }}


    <section class="container bg-light mb-3">

<div id="socialarea" class="hidden-sm-up">
  <ul id="sociallist">
    <li class="facebook_social"> <a href="http://www.facebook.com/" target="_new"><i class="fa fa-facebook"></i></a> </li>
    <li class="twitter_social"> <a href="https://twitter.com/" target="_new"><i class="fa fa-twitter"></i></a> </li>
    <li class="googleplus_social"> <a href="https://plus.google.com/" target="_new"><i class="fa fa-google-plus"></i></a> </li>
    <li class="linkedin_social"> <a href="http://www.linkedin.com/" target="_new"><i class="fa fa-linkedin"></i></a> </li>
    <li class="pinterest_social"> <a href="http://pinterest.com/" target="_new"><i class="fa fa-pinterest-p"></i></a> </li>
    <li class="instagram_social"> <a href="https://www.instagram.com//" target="_new"><i class="fa fa-instagram"></i></a> </li>
  </ul>

</div>


      <div class="container">
      <div class="row">
        <div class="col-md-9">
        <h3 lang="{{ app()->getLocale()  }}"><strong>{{ $info['title'] }} </strong></h3>
            
               <div class="mb-2">
                  <img src="/media/markalar/demo.png" alt="-" class="img-fluid" />
               </div>
               <div class="row">
    <div class="col-lg-12 mb-4">
        <div class="btn-group pull-right">
            <button type="button" id="incfont" class="btn btn-default"><i class="fa fa-font" aria-hidden="true"></i><i class="fa fa-plus"></i></button>
            <button type="button" id="decfont" class="btn btn-default"><i class="fa fa-font" aria-hidden="true"></i><i class="fa fa-minus"></i></button>
        </div>
    </div>
</div>
            <div class="fontsizer">
                {{ $info['content'] }}
            </div>

               <div class="row">
                <div class="col-12 col-md-12 SocialButtonList">
                    <hr class="tall">
                    <h3><strong>{{ __('general.shareit') }}</strong></h3>
                    <div dir="ltr">
                        <a target="_Blank" href="http://twitter.com/home?status=" title=" twitter_text " rel="external,nofollow" class="btn btn-twitter SocHref pFlipX"><i class="fa fa-twitter"></i> Twitter</a>

                        <a target="_Blank" href="https://www.facebook.com/sharer/sharer.php?u=" title=" facebook_text " rel="external,nofollow" class="btn btn-facebook SocHref pFlipX"><i class="fa fa-facebook"></i> Facebook</a>

                        <a target="_Blank" href="https://plus.google.com/share?url=" title=" googleplus_text " rel="external,nofollow" class="btn btn-googleplus SocHref pFlipX"><i class="fa fa-google-plus"></i> Google+</a>

                        <a target="_Blank" href="http://www.stumbleupon.com/submit?url=" title=" stumbleupon_text " rel="external,nofollow" data-placement="top" class="btn btn-stumbleupon SocHref pFlipX"><i class="fa fa-stumbleupon"></i> Stumbleupon</a>
                        
                        <a target="_Blank" href="https://www.linkedin.com/cws/share?url=" title=" linkedin_text " rel="external,nofollow" class="btn btn-linkedin SocHref pFlipX"><i class="fa fa-linkedin"></i> Linkedin</a>

                    </div>
                    <hr class="tall">
                </div>
            </div>
        </div>


    <div class="col-md-3">

        <h3><strong>Grup Markaları</strong></h3>

      <div class="col-lg-12 grupsirket">
        <div class="carousel slide" data-ride="carousel" data-interval="3000" id="slide">
          <div class="carousel-inner">

            <div class="item active">
            <a class="no-decoration grpmarka" href="#">
              <img src="/media/markalar/demo.png" class="img-fluid" alt="" />
            </a>
            <h5><a class="no-decoration grpmarka" href="#">Marka 1</a></h5>
              <p>Uğur Şirketler Grubu bünyesindeki Uğur Motorlu Araçlar A.Ş (UMA) kendi tescilli ve lokomotif markası "Mondial" ile 2004 yılında motosiklet sektöründe faaliyetlerine başlamıştır.</p>
            </div>

            <div class="item ">
            <a class="no-decoration grpmarka" href="#">
              <img src="/media/markalar/demo.png" class="img-fluid" alt="" />
            </a>
            <h5><a class="no-decoration grpmarka" href="#">Marka 2</a></h5>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>

            <div class="item ">
            <a class="no-decoration grpmarka" href="#">
              <img src="/media/markalar/demo.png" class="img-fluid" alt="" />
            </a>
            <h5><a class="no-decoration grpmarka" href="#">Marka 3</a></h5>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>

            <div class="item ">
            <a class="no-decoration grpmarka" href="#">
              <img src="/media/markalar/demo.png" class="img-fluid" alt="" />
            </a>
            <h5><a class="no-decoration grpmarka" href="#">Marka 4</a></h5>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>

            <div class="item ">
            <a class="no-decoration grpmarka" href="#">
              <img src="/media/markalar/demo.png" class="img-fluid" alt="" />
            </a>
            <h5><a class="no-decoration grpmarka" href="#">Marka 5</a></h5>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>


          </div>
        </div>

      </div>
        <div>
          <a data-slide="next" href="#slide" class="btn btn-default pull-right">›</a>
                  <a data-slide="prev" href="#slide" class="btn btn-default pull-right">‹</a>
        </div>
    </div>


      </div>
      </div>
    </section>

<footer>
         <div class="container row ml-auto mr-auto">
            <div class="col-md-2 mt-5 hidden-sm-up">
               <h3>Kurumsal</h3>
               <ul class="yan">
                  <li class="list-item"><a href="#">Hakkımızda</a></li>
                  <li class="list-item"><a href="#">Misyonumuz</a></li>
                  <li class="list-item"><a href="#">Vizyonumuz</a></li>
                  <li class="list-item"><a href="#">Tarihçe</a></li>
                  <li class="list-item"><a href="#">Gizlilik Politikası</a></li>
               </ul>
            </div>
            <div class="col-md-3 mt-5">
               <h3>Grup Şirketleri</h3>
               <ul class="yan">
                  <li class="list-item"><a href="#">Ertok Yapı Malzemeleri</a></li>
                  <li class="list-item"><a href="#">Yemenoğulları Hırdavat</a></li>
                  <li class="list-item"><a href="#">Yemenoğlu Gayrimenkul</a></li>
                  <li class="list-item"><a href="#">Öztaş Gayrimenkul</a></li>
                  <li class="list-item"><a href="#">Ges Madencilik</a></li>
                  <li class="list-item"><a href="#">Ertok Sigorta</a></li>
               </ul>
            </div>
            <div class="col-md-2 mt-5">
               <h3>Medya</h3>
               <ul class="yan">
                  <li class="list-item"><a href="#">Haberler</a></li>
                  <li class="list-item"><a href="#">Kurumsal Logolar</a></li>
               </ul>
            </div>
            <div class="col-md-3 mt-5 hidden-sm-up">
               <h3>İnsan Kaynakları</h3>
               <ul class="yan">
                  <li class="list-item"><a href="#">İnsan Kaynakları Başvuru</a></li>         
               </ul>
            </div>
            <div class="col-md-2 mt-5">
               <h3>İletişim</h3>
               <ul class="yan">
                  <li class="list-item"><a href="#">Adreslerimiz</a></li>
                  <li class="list-item"><a href="#">Bize Ulaşın</a></li>          
               </ul>
            </div>

            <div class="container mt-3 mb-3">
            <div class="col-4 ml-auto mr-auto justify-content-center">
             <span class="">2018 - Tüm Hakları Saklıdır.</span>
            </div>

            </div>
         </div>

      </footer>


@endsection

@section('js')



    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="//stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <script src="/js/jquery.backstretch.min.js"></script>
    <script src="/js/owl.carousel.js" type="text/javascript"></script>
    <script src="/js/scrolloverflow.js" type="text/javascript"></script>
    <script src="/js/jquery.fullPage.js" type="text/javascript"></script>
    <script src="/js/sbx.js"></script>



@stop

</body>