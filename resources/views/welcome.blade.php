@extends('layouts.app')

@section('content')
    <div class="container sbx-starter">
        <div class="row">
            <div class="col-md-8 mr-auto ml-auto">
                <div class="card card-default">
                    <div class="card-header">User Dash</div>

                    <div class="card-body">

                        @component('components.who')
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('exjquery')

@stop