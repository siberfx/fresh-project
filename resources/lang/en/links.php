<?php

return [

    'main' => '/en/',
    'corporate' => '/en/corporate/',
    'group' => '/en/coop/',
    'blog' => '/en/blog/',
    'hr' => '/en/hr',
    'media' => '/en/media/',
    'contact' => '/en/contact/',

    'main_nav' => 'Main Page',
    'corporate_nav' => 'About Us',
    'group_nav' => 'Cooperatives',
    'blog_nav' => 'Blog',
    'hr_nav' => 'Human Resources',
    'media_nav' => 'Media',
    'contact_nav' => 'Contact Us',
];