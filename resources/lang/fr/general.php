<?php

return [


    'mainpage'  => 'Main Page',
    'contactus' => 'Contact Us',
    'login'     => 'Login',
    'register'  => 'Register',
    'shareit'  => 'Share This',

];
