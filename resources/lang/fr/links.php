<?php

return [

    'main' => '/fr/',
    'corporate' => '/fr/corporate/',
    'group' => '/fr/coop/',
    'blog' => '/fr/blog/',
    'hr' => '/fr/hr',
    'media' => '/fr/media/',
    'contact' => '/fr/contact/',

    'main_nav' => 'Main Page',
    'corporate_nav' => 'About Us',
    'group_nav' => 'Cooperatives',
    'blog_nav' => 'Blog',
    'hr_nav' => 'Human Resources',
    'media_nav' => 'Media',
    'contact_nav' => 'Contact Us',
];