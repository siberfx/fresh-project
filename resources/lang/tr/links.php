<?php

return [

//    'main' => '/tr/',
//    'corporate' => '/tr/kurumsal/',
//    'group' => '/tr/grup-sirketler/',
//    'blog' => '/tr/haberler/',
//    'hr' => '/tr/ik',
//    'media' => '/tr/medya/',
//    'contact' => '/tr/iletisim/',

    'main' => '/tr/',
    'corporate' => '/tr/kurumsal/baslik',
    'group' => '/tr/grup-sirketler/baslik',
    'blog' => '/tr/haberler/baslik',
    'hr' => '/tr/ik/baslik',
    'media' => '/tr/medya/baslik',
    'contact' => '/tr/iletisim/baslik',

    'main_nav' => 'Ana Sayfa',
    'corporate_nav' => 'Kurumsal',
    'group_nav' => 'Grup Şirketler',
    'blog_nav' => 'Haberler',
    'hr_nav' => 'İnsan Kaynakları',
    'media_nav' => 'Medya',
    'contact_nav' => 'İLETİŞİM',
];