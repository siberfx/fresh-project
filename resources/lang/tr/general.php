<?php

return [

    'mainpage'  => 'Ana Sayfa',
    'coop'  => 'Grup Şirketler',
    'contactus' => 'İletişim',
    'login'     => 'Giriş yap',
    'register'  => 'Kayıt ol',
    'shareit'  => 'Bu Makaleyi Paylaş',

];
