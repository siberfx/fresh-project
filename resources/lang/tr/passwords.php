<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Şifreniz en az 6 karakter olmak zorundadır.',
    'reset' => 'Şifreniz sıfırlandı!',
    'sent' => 'Yeni şifrenizi içeren e-posta tarafınıza gönderilmiştir',
    'token' => 'Bu şifre doğrulama token kodu geçersiz.',
    'user' => "Bu e-posta adresine kayıtlı bir üye bulunamadı.",

];
