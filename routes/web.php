<?php

Route::get('/', function() {
    return redirect()->to(App::getLocale());
});

//Route::get('/users/logout', 'Auth\LoginController@userLogout')->name('tr.user.logout');

if (Request::segment(1) === 'tr') {
    Route::prefix('tr')->group(function () {
        App::setLocale('tr');
        Route::get('/', 'HomeAnyController@index')->name('tr.rooturl');
        Route::get('/kurumsal/{slug}', 'HomeAnyController@corporate')->name('tr.company');
        Route::get('/grup-sirketler/{slug}', 'HomeAnyController@corporate')->name('tr.corporate');
        Route::get('/iletisim/{slug}', 'HomeAnyController@corporate')->name('tr.contact');
    });
}

if (Request::segment(1) === 'en') {
    Route::prefix('en')->group(function () {
        App::setLocale('en');
        Route::get('/', 'HomeAnyController@index')->name('en.rooturl');
        Route::get('/corporate/{slug}', 'HomeAnyController@corporate')->name('en.company');
        Route::get('/coop/{slug}', 'HomeAnyController@corporate')->name('en.corporate');
        Route::get('/contact/{slug}', 'HomeAnyController@corporate')->name('en.contact');
    });
}



if (Request::segment(1) === 'fr') {
    Route::prefix('fr')->group( function() {
        App::setLocale('fr');
        Route::get('/', 'HomeAnyController@index')->name('fr.rooturl');
        Route::get('/corporate/{slug}', 'HomeAnyController@corporate')->name('fr.company');
        Route::get('/coop/{slug}', 'HomeAnyController@corporate')->name('fr.corporate');
        Route::get('/contact/{slug}', 'HomeAnyController@corporate')->name('fr.contact');
    });
}


Auth::routes();
Route::prefix('admin')->group( function() {

    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/pagelist', 'AdminController@pages_list')->name('admin.pages.list');
    Route::get('/page_view/{id}', 'AdminController@pages_view')->name('admin.pages.view');
    Route::get('/page_edit/{id}', 'AdminController@pages_edit')->name('admin.pages.edit');
    Route::post('/page_save/{id}', 'AdminController@pages_save')->name('admin.pages.save');
    Route::get('/contact', 'AdminController@contact')->name('admin.contact');
    Route::post('/update_contact/{id}', 'AdminController@contact_update'); // Updating contact

    Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    // password reset part
    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
});

